﻿namespace BubbleMusic
{
    public interface UIMangerHandler
    {
        void Show(Define.UIName uIName);
        void Hide(Define.UIName uIName);
    }
}