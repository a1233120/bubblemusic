﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BubbleMusic
{
    public class UIManger : IManger, UIMangerHandler, IEventListener
    {
        public static UIManger instance { get; private set; } = null;
        private string UI_GAMEPANEL_ROOT = "UI/{0}/{0}";
        Dictionary<Define.UIName, ViewHandler> UIList;
        private GameObject uiRootNode;
        private readonly string uiRootNodeTag = "UICanvas";

        public static IManger CreateManger()
        {
            if (instance == null)
            {
                instance = new UIManger();
            }

            return instance;
        }

        private UIManger() { }

        public void Show(Define.UIName uIName)
        {
            if (!UIList.ContainsKey(uIName))
            {
                string path = string.Format(UI_GAMEPANEL_ROOT, uIName.ToString());
#if DEBUG_MODE
                Debug.Log(path);
#endif
                BaseView gameObject = Resources.Load<BaseView>(path);

                if (gameObject == null)
                {
#if DEBUG_MODE
                    Debug.Log("is not message" + uIName.ToString());
#endif
                    return;
                }

                UIList.Add(uIName, Object.Instantiate(gameObject, uiRootNode.transform));
            }

            UIList[uIName].Show();
        }

        public void Hide(Define.UIName uIName)
        {
            if (UIList.ContainsKey(uIName))
            {
                UIList[uIName].Hide();
            }
        }       

        private void RemoveAllUIForLoading()
        {
            foreach (var item in UIList)
            {
                if (item.Key != Define.UIName.Loading)
                {
                    Object.Destroy((Object)item.Value);
                    UIList.Remove(item.Key);
                }
            }
        }

        public void OnAwake()
        {
            UIList = new Dictionary<Define.UIName, ViewHandler>();
            uiRootNode = GameObject.FindWithTag(uiRootNodeTag);
        }

        public void OnStart()
        {
            EventManger.instance.AddListener(Define.EventType.ChangeToGame, OnEvent);
            Show(Define.UIName.PreLoad);
        }

        public void OnDestory()
        {
            foreach (var view in UIList)
            {
                view.Value.OnDestory();
            }
        }

        public void OnEvent(Define.EventType eventType)
        {
            switch (eventType)
            {
                case Define.EventType.ChangeToGame:
                    Hide(Define.UIName.Lobby);
                    Show(Define.UIName.Game);
                    ModuleManger.instance.GetModule<GameModule.GameController>().EnterGame(Define.choose_music);
                    Debug.Log("Change!!!!!!");
                    break;
            }
        }
    }
}