﻿using UnityEngine;
using BubbleMusic;

public class CameraManger : MonoBehaviour
{
    public float baseWidth = 1024;
    public float baseHeight = 768;
    public float baseOrthographicSize = 5;

    void Awake()
    {
        float newOrthographicSize = (float)Screen.height / (float)Screen.width * this.baseWidth / this.baseHeight * this.baseOrthographicSize;
        Camera.main.orthographicSize = Mathf.Max(newOrthographicSize, this.baseOrthographicSize);
        Input.multiTouchEnabled = true; // 開啟多點觸擊
    }

    void Update()
    {
        // 暫停不給點擊
        if (Time.timeScale == 0)
            return;

        if(Input.GetKeyDown(KeyCode.Q))
            ModuleManger.instance.GetModule<BubbleMusic.GameModule.GameController>().KeyPress(KeyCode.Q);
        if(Input.GetKeyDown(KeyCode.W))
            ModuleManger.instance.GetModule<BubbleMusic.GameModule.GameController>().KeyPress(KeyCode.W);
        if (Input.GetKeyDown(KeyCode.O))
            ModuleManger.instance.GetModule<BubbleMusic.GameModule.GameController>().KeyPress(KeyCode.O);
        if (Input.GetKeyDown(KeyCode.P))
            ModuleManger.instance.GetModule<BubbleMusic.GameModule.GameController>().KeyPress(KeyCode.P);

#if UNITY_EDITOR
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.tag == "note")
                {
                    hit.transform.GetComponent<BubbleMusic.GameModule.GameINote>().OnTouchBegin();
                }
            }
        }
#else
        // 遊戲觸控, 請勿更動
        int touchCount = Input.touchCount;
        if (touchCount > 0)
        {
            for (int i = 0; i < touchCount; i++)
            {
                Touch touch = Input.GetTouch(i);
                if (touch.phase == TouchPhase.Began)
                {
                    Ray ray = Camera.main.ScreenPointToRay(touch.position);
                    RaycastHit hit;
                    if (Physics.Raycast(ray, out hit))
                    {
                        if (hit.transform.tag == "note")
                        {
                            hit.transform.GetComponent<BubbleMusic.GameModule.GameINote>().OnTouchBegin();
                        }
                    }
                }
            }
        }
#endif
    }
}
