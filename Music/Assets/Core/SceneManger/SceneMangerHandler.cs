﻿namespace BubbleMusic
{
    public interface SceneMangerHandler
    {
        void ChangeScene(Define.SceneName sceneName);
        Define.SceneName GetCurrentScene();
    }
}
