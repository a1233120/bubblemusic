﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BubbleMusic {
    public class SceneManger : IManger, SceneMangerHandler
    {
        public static SceneManger instance { get; private set; } = null;
        private readonly Define.SceneName currentScene = Define.SceneName.PreLoad;

        public static IManger CreateManger()
        {
            if (instance == null)
            {
                instance = new SceneManger();
            }

            return instance;
        }

        private SceneManger() { }

        public void OnAwake()
        {
            
        }

        public void OnStart()
        {
            
        }

        public void OnDestory()
        {
            
        }

        public void ChangeScene(Define.SceneName sceneName)
        {         
            if(sceneName == Define.SceneName.Game)
                EventManger.instance.PostNotification(Define.EventType.ChangeToGame);
        }

        public Define.SceneName GetCurrentScene()
        {
            return currentScene;
        }
    }
}