﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Linq;
using UnityEngine;

namespace BubbleMusic {
    public class FileManger : IManger, FileMangerHandler
    {
        private string SHEET_FILE_PATH = "Sheet/{0}";
        private string NOTES_FILE_PATH = "Notes/{0}";
        private string INFO_FILE_PATH = "Info/{0}";
        private string SONG_FILE_PATH = "UI/Lobby/SongData";

        public static FileManger instance { get; private set; } = null;

        public static IManger CreateManger()
        {
            if (instance == null)
            {
                instance = new FileManger();
            }

            return instance;
        }

        private FileManger() {}

        public void OnAwake()
        {

        }

        public void OnStart()
        {

        }

        public void OnDestory()
        {

        }

        public XElement ReadXmlFile(string fileName)
        {
            TextAsset t = Resources.Load<TextAsset>(string.Format(SHEET_FILE_PATH, fileName));                     
            return XDocument.Parse(t.text).Root;        
        }

        public XElement ReadNotesXmlFile(string fileName)
        {
            TextAsset t = Resources.Load<TextAsset>(string.Format(NOTES_FILE_PATH, fileName + "Notes"));
            return XDocument.Parse(t.text).Root;           
        }

        public XElement ReadInfoXmlFile(string fileName)
        {
            TextAsset t = Resources.Load<TextAsset>(string.Format(INFO_FILE_PATH, fileName));
            return XDocument.Parse(t.text).Root;
        }

        public XDocument ReadSongDateXmlFile()
        {
            TextAsset t = Resources.Load<TextAsset>(SONG_FILE_PATH);
            return XDocument.Parse(t.text);
        }
    }
}