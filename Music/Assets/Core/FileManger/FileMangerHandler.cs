﻿namespace BubbleMusic
{
    public interface FileMangerHandler
    {
        System.Xml.Linq.XElement ReadXmlFile(string fileName);
        System.Xml.Linq.XElement ReadNotesXmlFile(string fileName);
        System.Xml.Linq.XElement ReadInfoXmlFile(string fileName);
        System.Xml.Linq.XDocument ReadSongDateXmlFile();
    }
}