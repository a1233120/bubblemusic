﻿namespace BubbleMusic
{
    public static class Define
    {
        public static string Version { get; private set; } = "1.0";
        public static Eevirmoent Eev { get; private set; } = Eevirmoent.Development;
        public static string choose_music;

        public enum Eevirmoent
        {
            Development,
            Production
        }

        public enum SceneName
        {
            PreLoad,
            Lobby,
            Game
        }

        public enum UIName
        {
            PreLoad,
            Lobby,
            Ready,
            Game,
            Loading,
            Result,
            Pause,
        }

        public enum EventType
        {
            Default,
            ChangeToGame,   // 轉景到遊戲場景內
            GameEnd,        // 遊戲結束
            UpdateScore,    // 通知刷新分數
            UpdateCombo,    // 通知刷新Combo
            HitPerfect,     // 通知打擊判定Perfect
            HitGood,        // 通知打擊判定Good
            HitBad,         // 通知打擊判定Bad
            HitMiss,        // 通知打擊判定Miss
        }

        public enum Effect
        {
            Ready,
            Button,            
        }

        public enum BackMusic
        {
            Start
        }
    }
}