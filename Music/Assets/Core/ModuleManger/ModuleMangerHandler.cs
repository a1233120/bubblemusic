﻿namespace BubbleMusic
{
    public interface ModuleMangerHandler
    {
        T GetModule<T>() where T : class, IModule;
    }
}