﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BubbleMusic
{
    public class ModuleManger : IManger, IProcess, ModuleMangerHandler
    {
        public static ModuleManger instance { get; private set; } = null;
        Dictionary<System.Type, IModule> modules = new Dictionary<System.Type, IModule>();

        public static IManger CreateManger()
        {
            if (instance == null)
            {
                instance = new ModuleManger();
            }

            return instance;
        }

        private ModuleManger() { }

        public T GetModule<T>() where T : class, IModule
        {
            if (modules.ContainsKey(typeof(T)))
            {
                return modules[typeof(T)] as T;
            }

            return null;
        }

        public void OnAwake()
        {
            CreateAllModule();
        }

        private void CreateAllModule()
        {
            modules.Add(typeof(GameModule.GameController), GameModule.GameController.CreateModule());
            modules.Add(typeof(RankModule.RankController), RankModule.RankController.CreateModule());
        }

        public void OnDestory()
        {
            foreach (var module in modules)
            {
                module.Value.OnStop();
            }
        }

        public void OnStart()
        {

        }

        public void Process(float deltaTime)
        {
            foreach (var module in modules)
            {
                module.Value.OnUpdate(deltaTime);
            }
        }
    }
}