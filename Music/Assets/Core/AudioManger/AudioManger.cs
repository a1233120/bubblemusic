﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BubbleMusic {
    public class AudioManger : IManger, IAudioManger
    {
        public static AudioManger instance { get; private set; } = null;
        SoundManger manger;
        Sound background;
        Sound game;
        Sound effect;

        public static IManger CreateManger()
        {
            if (instance == null)
            {
                instance = new AudioManger();
            }

            return instance;
        }

        private AudioManger() { }

        public void OnAwake()
        {
            manger = SoundManger.Main;
        }

        public void OnStart()
        {
            // todo 記得加回來
            //PlayBackgroundMusic("bluebird");
        }

        public void OnDestory()
        {

        }

        public void PlayBackgroundMusic(string name)
        {
            background = manger.PlayNewSound(name, loop: true);       
        }

        public void StopBackgroundMusic()
        {
            if(background != null)
                background.playing = false;
        }

        public void PlayGameMusic(string name, Action<Sound> callback = null)
        {
            game = manger.PlayNewSound(name, callback: callback);            
        }

        public bool IsGameMusicStop()
        {
            if (game != null)
            {                
                return game.finished;
            }
            return false;
        }

        public void PauseGameMusic()
        {
            if(game != null)
                game.playing = false;
        }

        public void ResumeGameMusic()
        {
            if (game != null)
                game.playing = true;
        }

        public void StopGameMusic()
        {
            if(game != null)
                game.Finish();
        }

        public void PlayEffectMusic(string name)
        {
            effect = manger.PlayNewSound(name);           
        }
    }
}