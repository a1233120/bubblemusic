﻿namespace BubbleMusic
{
    public interface IAudioManger
    {
        void PlayBackgroundMusic(string name); // 播放背景音樂
        bool IsGameMusicStop();                 // 遊戲音樂是否結束了
        void StopBackgroundMusic();             // 停止背景音樂
        void PlayGameMusic(string name, System.Action<Sound> callback = null);       // 播放遊戲音樂
        void StopGameMusic();                   // 停止遊戲音樂
        void PauseGameMusic();                   // 暫停遊戲音樂
        void ResumeGameMusic();                  // 恢復遊戲音樂
        void PlayEffectMusic(string name);     // 播放特效音樂
    }
}