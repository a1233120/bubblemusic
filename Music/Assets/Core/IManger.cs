﻿namespace BubbleMusic
{
    public interface IManger
    {
        // 一個Manger需要擁有的方法
        // OnAwake 將各個Manger 初始化
        // OnStart 將各個Manger 第一次啟動的時候要做的事情
        // Process 每個狀態下Manger要做的事情
        // 當物件要銷毀時 每個Manger要做的事情
        void OnAwake();
        void OnStart();
        void OnDestory();
    }
}