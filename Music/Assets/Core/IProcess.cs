﻿namespace BubbleMusic
{
    public interface IProcess
    {
        // 用於狀態機
        void Process(float deltaTime);
    }
}