﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BubbleMusic{
    public class AppManger : MonoBehaviour
    {
        List<IManger> Mangers;
        private float m_LastUpdateShowTime = 0f;    //上一次更新帧率的时间;

        private float m_UpdateShowDeltaTime = 0.01f;//更新帧率的时间间隔;

        private int m_FrameUpdate = 0;//帧数;

        private float m_FPS = 0;
        private GUIStyle style;

        private void Awake()
        {
            //DontDestroyOnLoad(gameObject);
            Application.targetFrameRate = -1;
            Mangers = new List<IManger>();
            CreatAllManger();
            foreach (var manger in Mangers)
            {
                manger.OnAwake();
            }
        }

        private void CreatAllManger()
        {
            Mangers.Add(UIManger.CreateManger());
            Mangers.Add(EventManger.CreateManger());           
            Mangers.Add(SceneManger.CreateManger());           
            Mangers.Add(AudioManger.CreateManger());
            Mangers.Add(FileManger.CreateManger());
            Mangers.Add(ModuleManger.CreateManger());
        }

        // Start is called before the first frame update
        private void Start()
        {
            style = new GUIStyle();
            style.fontSize = 50;
            m_LastUpdateShowTime = Time.realtimeSinceStartup;
            foreach (var manger in Mangers)
            {
                manger.OnStart();
            }
        }

        // Update is called once per frame
        private void Update()
        {
            m_FrameUpdate++;
            if (Time.realtimeSinceStartup - m_LastUpdateShowTime >= m_UpdateShowDeltaTime)
            {
                m_FPS = m_FrameUpdate / (Time.realtimeSinceStartup - m_LastUpdateShowTime);
                m_FrameUpdate = 0;
                m_LastUpdateShowTime = Time.realtimeSinceStartup;
            }

            foreach (var manger in Mangers)
            {
                IProcess _manger = manger as IProcess;
                if(_manger != null)
                    _manger.Process(Time.deltaTime);
            }
        }

        void OnGUI()
        {
            //GUI.Label(new Rect(Screen.width / 2, 0, 500, 500), "FPS: " + m_FPS, style);
        }

        private void OnDestroy()
        {
            foreach (var manger in Mangers)
            {
                manger.OnDestory();
            }
            Mangers.Clear();
        }
    }
}