﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BubbleMusic
{
    public class EventManger : IManger, EventMangerHandler
    {
        public static EventManger instance { get; private set; } = null;
        private Dictionary<Define.EventType, List<OnEvent>> listeners;

        public static IManger CreateManger()
        {
            if (instance == null)
            {
                instance = new EventManger();
            }

            return instance;
        }

        private EventManger() { }

        public void OnAwake()
        {
            listeners = new Dictionary<Define.EventType, List<OnEvent>>();
        }

        public void OnStart()
        {
           
        }

        public void OnDestory()
        {
            RemoveRedundancies();
        }

        public void AddListener(Define.EventType eventType, OnEvent listener)
        {
            List<OnEvent> ListenList = null;

            if (listeners.TryGetValue(eventType, out ListenList))
            {
                ListenList.Add(listener);
                return;
            }

            ListenList = new List<OnEvent>();
            ListenList.Add(listener);
            listeners.Add(eventType, ListenList);
        }

        public void PostNotification(Define.EventType eventType)
        {
            List<OnEvent> ListenList = null;

            if (!listeners.TryGetValue(eventType, out ListenList))
            {
                return;
            }

            foreach (var item in ListenList)
            {
                if (!item.Equals(null))
                {
                    item(eventType);
                }
            }
        }

        public void RemoveEvent(Define.EventType eventType)
        {
            listeners.Remove(eventType);
        }

        public void RemoveRedundancies()
        {
            listeners.Clear();
        }
    }
}