﻿namespace BubbleMusic
{
    public delegate void OnEvent(Define.EventType eventType);

    public interface EventMangerHandler
    {
        void AddListener(Define.EventType eventType, OnEvent listener);
        void PostNotification(Define.EventType eventType);
        void RemoveEvent(Define.EventType eventType);
        void RemoveRedundancies();
    }
}