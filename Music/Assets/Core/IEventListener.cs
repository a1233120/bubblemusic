﻿namespace BubbleMusic
{
    public interface IEventListener
    {
        void OnEvent(Define.EventType eventType);
    }
}