﻿namespace BubbleMusic
{
    public interface ViewHandler
    {
        void Show();
        void Hide();
        void OnDestory();
    }
}