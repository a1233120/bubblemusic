﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BubbleMusic
{
    public class UIReady : BaseView
    {
        AnimationClip clip3;
        public Animation ani;
        // Start is called before the first frame update
        void Start()
        {
            clip3 = transform.GetComponent<Animation>().GetClip("ready");
            AnimationEvent aevent = new AnimationEvent();
            aevent.functionName = "HideObject";
            aevent.time = 2.05f;
            if (clip3)
                clip3.AddEvent(aevent);
        }

        // Update is called once per frame
        void Update()
        {

        }

        public override void Show()
        {
            base.Show();
            ani.Play("ready");
            AudioManger.instance.PlayEffectMusic(Define.Effect.Ready.ToString());
        }

        private void HideObject()
        {
            UIManger.instance.Hide(Define.UIName.Ready);
            ModuleManger.instance.GetModule<GameModule.GameController>().PlayGame();
        }
    }
}