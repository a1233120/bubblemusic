﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BubbleMusic;
using BubbleMusic.GameModule;
using UnityEngine.UI;

public class UIPause : BaseView
{
    public Button btn_continue;
    public Button btn_again;
    public Button btn_exit;

    // Start is called before the first frame update
    void Start()
    {
        btn_continue.onClick.AddListener(() => { OnContinue(); });
        btn_again.onClick.AddListener(() => { OnAgain(); });
        btn_exit.onClick.AddListener(() => { OnExit(); });
    }

    void OnContinue()
    {
        Time.timeScale = 1;
        UIManger.instance.Hide(Define.UIName.Pause);
        ModuleManger.instance.GetModule<GameController>().ResumeGame();
    }

    void OnExit()
    {
        Time.timeScale = 1;
        UIManger.instance.Hide(Define.UIName.Pause);
        ModuleManger.instance.GetModule<GameController>().EndGame();
    }

    void OnAgain()
    {
        Time.timeScale = 1;
        UIManger.instance.Hide(Define.UIName.Pause);
        ModuleManger.instance.GetModule<GameController>().ClearAll();
        SceneManger.instance.ChangeScene(Define.SceneName.Game);
    }
}
