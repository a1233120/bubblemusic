﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BubbleMusic;

public class UIResult : BaseView
{
    public Button m_btn_again;
    public Button m_btn_menu;
    public Text m_score;
    public Text m_combo;
    public Text m_good;
    public Text m_bad;
    public Text m_miss;
    public Text m_perfect;


    public override void Show()
    {
        base.Show();        
        SetScore(ModuleManger.instance.GetModule<BubbleMusic.RankModule.RankController>().GetGameScore());
        SetPerfect(ModuleManger.instance.GetModule<BubbleMusic.RankModule.RankController>().GetPerfectHits());
        SetGood(ModuleManger.instance.GetModule<BubbleMusic.RankModule.RankController>().GetGoodHits());
        SetBad(ModuleManger.instance.GetModule<BubbleMusic.RankModule.RankController>().GetBadHits());
        SetMiss(ModuleManger.instance.GetModule<BubbleMusic.RankModule.RankController>().GetMissHits());
        SetCombo(ModuleManger.instance.GetModule<BubbleMusic.RankModule.RankController>().GetTotalCombo());
    }

    public void PlayAgain()
    {
        UIManger.instance.Hide(Define.UIName.Result);
        SceneManger.instance.ChangeScene(Define.SceneName.Game);
    }

    public void GoMenu()
    {        
        UIManger.instance.Hide(Define.UIName.Game);
        UIManger.instance.Hide(Define.UIName.Result);
        UIManger.instance.Show(Define.UIName.Lobby);
        ResetRecord();
    }

    private void ResetRecord()
    {
        m_score.text = "0";
    }

    private void SetScore(int score)
    {
        if(score >= 0 )
            m_score.text = string.Format("{0}", score) ;
    }

    private void SetCombo(int combo)
    {
        if (combo >= 0)
            m_combo.text = string.Format("{0}", combo);
    }

    private void SetPerfect(int perfect)
    {
        if (perfect >= 0)
            m_perfect.text = string.Format("{0}", perfect);
    }

    private void SetGood(int good)
    {
        if (good >= 0)
            m_good.text = string.Format("{0}", good);
    }

    private void SetBad(int bad)
    {
        if (bad >= 0)
            m_bad.text = string.Format("{0}", bad);
    }

    private void SetMiss(int miss)
    {
        if (miss >= 0)
            m_miss.text = string.Format("{0}", miss);
    }
}
