﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BubbleMusic
{
    public abstract class BaseView : MonoBehaviour, ViewHandler
    {
        public virtual void Hide()
        {
            gameObject.SetActive(false);
        }

        public virtual void Show()
        {
            gameObject.SetActive(true);
            gameObject.transform.SetAsLastSibling();
        }

        public void OnDestory()
        {
            Destroy(this);
            print("view Destory");
        }
    }
}