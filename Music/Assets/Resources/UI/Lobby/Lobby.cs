﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Xml.Linq;
using System.IO;

namespace BubbleMusic
{
    public class Lobby : MonoBehaviour
    {
        public GameObject Content;
        public Button button;
        private Dictionary<string, string> song_map;
        private List<Button> SongListBtn = new List<Button>();
        private int Quantity;
        private List<XDocument> Song = new List<XDocument>();
        private XDocument SongData;


        void Awake()
        {
            song_map = new Dictionary<string, string>();
            // 讀取檔案
            SongData = FileManger.instance.ReadSongDateXmlFile();
        }

        // Use this for initialization
        void Start()
        {
            LoadSongData();
            CreateBtn();
        }

        public void CreateBtn()
        {                        
            string fname = "UI/Lobby/";
            for (int i = 1; i <= Quantity; i++)
            {
                SongListBtn.Add(Instantiate(button));
                SongListBtn[i - 1].transform.SetParent(Content.transform);
                SongListBtn[i - 1].name = (i).ToString();
                SongListBtn[i - 1].GetComponent<Image>().sprite = Resources.Load<Sprite>(fname + i.ToString());
                //SongListBtn[i-1].GetComponentInChildren<Text>().text = SongData.Element("document").Element("song" + i.ToString()).Value.ToString();
            }
            int n = 0;
            foreach (var song in song_map)
            {
                SetTouchListener(SongListBtn[n], song.Key.ToString());
                n++;
            }
        }

        private void LoadSongData()
        {                        
            Quantity = int.Parse(SongData.Element("document").Element("Quantity").Value);

            // 取得檔名(key)
            // 取得譜面(value)
            for (int i = 1; i <= Quantity; i++)
            {
                song_map.Add(SongData.Element("document").Element("song" + i.ToString()).Value, SongData.Element("document").Element("value" + i.ToString()).Value);
            }
        }


        private void SetTouchListener(Button button, string songName)
        {
            Button tempbutton = button.transform.GetComponent<Button>();
            tempbutton.onClick.AddListener(delegate ()
            {
                Debug.Log("SongValue: " + song_map[songName]);                
                Define.choose_music = songName;
                SceneManger.instance.ChangeScene(Define.SceneName.Game);
            });
        }

        // Update is called once per frame
        void Update()
        {

        }


    }
}