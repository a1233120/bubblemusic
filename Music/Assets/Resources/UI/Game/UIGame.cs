﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BubbleMusic {
    public class UIGame : BaseView, IEventListener
    {
        public Text m_socre;
        public Button m_btn_pause;
        public Text m_combo;
        public Image img_combo;

        void Start()
        {
            EventManger.instance.AddListener(Define.EventType.UpdateScore, OnEvent);
            EventManger.instance.AddListener(Define.EventType.UpdateCombo, OnEvent);
            m_btn_pause.onClick.AddListener(() => { PauseGame(); });
        }

        public void OnEvent(Define.EventType eventType)
        {
            switch (eventType)
            {
                case Define.EventType.UpdateCombo:
                    SetGameCombo( ModuleManger.instance.GetModule<RankModule.RankController>().GetNowCombo() );
                    break;
                case Define.EventType.UpdateScore:
                    SetGameScore( ModuleManger.instance.GetModule<RankModule.RankController>().GetGameScore() );
                    break;
                default:
                    break;
            }
        }

        private void PauseGame()
        {
            ModuleManger.instance.GetModule<GameModule.GameController>().PauseGame();
            UIManger.instance.Show(Define.UIName.Pause);
        }

        private void SetGameScore(int score)
        {
            m_socre.text = string.Format("{0:0000000}", score);
        }

        private void SetGameCombo(int combo)
        {
            m_combo.enabled = combo > 0?true:false;
            img_combo.enabled = combo > 0?true:false;
            m_combo.text = string.Format("{0}", combo);
        }
    }
}