﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BubbleMusic;

public class UIPreLoad : BaseView
{
    public GameObject PreLoad;
    public Animation ani;
    public Button m_btn_play;
    public GameObject BubbleUP;
    private bool canShowLobby = false;
    private bool canClickPlay = true;
    private float loadingDT = 0.0f;
    // Start is called before the first frame update
    void Awake()
    {
        m_btn_play.onClick.AddListener(()=> { ShowBubbleUP(); });
    }

    void Start()
    {
    
    }

    // Update is called once per frame
    void Update()
    {
        if (!canShowLobby)
        {
            return;
        }

        if (loadingDT < 3)
        {
            loadingDT += Time.deltaTime;            
            return;
        }
        loadingDT = 0;
        canShowLobby = !canShowLobby;
        UIManger.instance.Hide(Define.UIName.PreLoad);
        UIManger.instance.Show(Define.UIName.Lobby);
    }

    public override void Show()
    {
        base.Show();
        ani.Play("play");
        AudioManger.instance.PlayBackgroundMusic(Define.BackMusic.Start.ToString());
    }

    public override void Hide()
    {
        AudioManger.instance.StopBackgroundMusic();
        base.Hide();
    }

    private void ShowBubbleUP()
    {
        if (!canClickPlay)
            return;
        canClickPlay = false;
        GameObject.Instantiate(BubbleUP,this.transform.position,this.transform.rotation).transform.SetParent(PreLoad.transform);        
        Invoke("ShowLobby",2);
    }

    private void ShowLobby()
    {
        canShowLobby = !canShowLobby;
        UIManger.instance.Show(Define.UIName.Loading);
    }
}
