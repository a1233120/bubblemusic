﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BubbleMusic.GameModule
{
    public class GameINote : MonoBehaviour, INote
    {
        private const string RECIVER_TAG = "reciver";           // 接收器的Tag              
        private GameSheetBuilder sheetBuilder;                  // 譜面建置器
        private float beat;                                     // 此Note的節奏點
        private Vector3 finalpos;                               // 結束的位置

        [HideInInspector]
        public bool canMove;                                   // 是否可以移動
        [HideInInspector]
        public bool canBeTouch;                                // 是否可以觸碰
        [HideInInspector]
        public bool isBadTouch;                                // 是否是bad的點擊
        [HideInInspector]
        public bool isGoodTouch;                               // 是否是Good的點擊
        [HideInInspector]
        public bool isPerfectTouch;                            // 是否是Perfect的點擊

        void Awake()
        {
            this.canMove = true;
            this.canBeTouch = false;
            this.isBadTouch = false;
            this.isGoodTouch = false;
            this.isPerfectTouch = false;            
        }

        void Update()
        {
            if (!canMove)
                return;
            transform.position = new Vector2(
                0.0f + (finalpos.x - 0.0f) * (1f - (beat - sheetBuilder.songPosition / sheetBuilder.secPerBeat) / sheetBuilder.BeatsShownOnScreen),
                     4.5f + (finalpos.y - 4.5f) * (1f - (beat - sheetBuilder.songPosition / sheetBuilder.secPerBeat) / sheetBuilder.BeatsShownOnScreen));
#if UNITY_EDITOR
            //if (canBeTouch && transform.position.y < finalpos.y)
            //    this.OnTouchBegin();
#endif
        }

        private void OnBecameInvisible()
        {
            Destroy(gameObject);
        }

        public void SetTargetPos(Vector3 vector3)
        {
            finalpos = vector3;
        }

        public void SetSheetBuilder(GameSheetBuilder sheetBuilder)
        {
            this.sheetBuilder = sheetBuilder;
        }

        public void SetBeat(float beat)
        {
            this.beat = beat;
        }

        public void OnTouchBegin()
        {
            if (!canBeTouch)
                return;

            gameObject.transform.GetComponent<Collider>().enabled = false;

            if (isPerfectTouch)
            {
                canBeTouch = !canBeTouch;
                canMove = !canMove;
                PlayPerfect();
                return;
            }

            if (isGoodTouch)
            {
                canBeTouch = !canBeTouch;
                canMove = !canMove;
                PlayGood();
                return;
            }

            if (isBadTouch)
            {
                canBeTouch = !canBeTouch;
                canMove = !canMove;
                PlayBad();
                return;
            }
        }

        private void PlayPerfect()
        {
            PlayBroken();
            GameEffectFactory.instance.PlayPerfect(gameObject.transform.position, gameObject);
            EventManger.instance.PostNotification(Define.EventType.HitPerfect);
        }
        
        private void PlayGood()
        {
            PlayBroken();
            GameEffectFactory.instance.PlayGood(gameObject.transform.position, gameObject);
            EventManger.instance.PostNotification(Define.EventType.HitGood);
        }

        private void PlayBad()
        {
            PlayBroken();
            GameEffectFactory.instance.PlayBad(gameObject.transform.position, gameObject);
            EventManger.instance.PostNotification(Define.EventType.HitBad);
        }

        public void PlayMiss()
        {
            GameEffectFactory.instance.PlayMiss(gameObject.transform.position, gameObject);
            EventManger.instance.PostNotification(Define.EventType.HitMiss);
        }

        private void PlayBroken()
        {
            GameEffectFactory.instance.PlayBroken(gameObject.transform.position, gameObject);
        }
    }
}