﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BubbleMusic.GameModule
{
    public class GameItemFactory : IGameItemFactory
    {
        public const string GAME_PERFABS_PATH = "Module/Game/{0}";
        

        public Transform CreateGameItem(string name)
        {
            GameObject r = Resources.Load<GameObject>(string.Format(GAME_PERFABS_PATH, name));
            Transform result = GameObject.Instantiate(r).transform;
            return result;
        }
    }
}