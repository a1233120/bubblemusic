﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BubbleMusic.GameModule {
    public class EffectNode : MonoBehaviour, IEffectNode
    {
        GameObject note;
        AnimationClip clip3;
        AnimationClip clip;
        public Animation ani;

        private void Awake()
        {

        }

        void Start()
        {
            clip3 = transform.GetComponent<Animation>().GetClip("bad");
            AnimationEvent aevent = new AnimationEvent();
            aevent.functionName = "HideObject";
            aevent.time = 0.44f;
            if (clip3)
                clip3.AddEvent(aevent);

            clip = transform.GetComponent<Animation>().GetClip("BrokenBubble");
            AnimationEvent aevent2 = new AnimationEvent();
            aevent2.functionName = "HideBroken";
            aevent2.time = 0.35f;
            if (clip)
                clip.AddEvent(aevent2);

        }

        private void Reset()
        {

        }

        private void HideObject()
        {        
            if (note)
                Destroy(note);
            if (gameObject)
                gameObject.transform.position = new Vector3(1000,1000,0);
        }

        private void HideBroken()
        {
            if (gameObject)
                gameObject.transform.position = new Vector3(1000, 1000, 0);
        }

        public void PlayBad(GameObject note)
        {            
            this.note = note;            
            ani.Play("bad");
        }

        public void PlayGood(GameObject note)
        {
            this.note = note;
            ani.Play("bad");
        }

        public void PlayMiss(GameObject note)
        {            
            this.note = note;
            ani.Play("bad");
        }

        public void PlayPerfect(GameObject note)
        {
            this.note = note;
            ani.Play("bad");
        }

        public void PlayBroken(GameObject note)
        {
            this.note = note;
            this.note.SetActive(false);
            ani.Play("BrokenBubble");
        }
    }
}