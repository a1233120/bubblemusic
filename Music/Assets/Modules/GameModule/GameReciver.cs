﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BubbleMusic.GameModule
{
    public class GameReciver : MonoBehaviour, IReciver
    {
        private List<Collider> notes;

        private void Awake()
        {
            notes = new List<Collider>();
        }

        public Vector3 GetPosition()
        {
            return gameObject.transform.position;
        }

        public void SetPosition(Vector3 vector3)
        {
            if(vector3 != null)
                gameObject.transform.position = vector3;
        }

        public void OnPress()
        {           
            foreach (var note in notes)
            {
                if (note != null)
                {
                    note.GetComponent<GameINote>().OnTouchBegin();
                    notes.RemoveAt(0);
                    break;
                }
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.tag == "note")
                notes.Add(other);                
        }
    }
}