﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BubbleMusic.GameModule {
    public class GameTriggerNode : MonoBehaviour
    {
        private const string PERFECT_TAG = "PerfectCoilder";
        private const string GOOD_TAG = "GoodCoilder";
        private const string BAD_TAG = "BadCoilder";
        private GameINote note;
        // Start is called before the first frame update
        void Start()
        {
            note = gameObject.GetComponentInParent<GameINote>();
        }

        // Update is called once per frame
        void Update()
        {

        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.tag == BAD_TAG)
            {
                note.canBeTouch = !note.canBeTouch;
                note.isBadTouch = !note.isBadTouch;
            }
            else if (other.tag == GOOD_TAG)
            {
                note.isGoodTouch = !note.isGoodTouch;
            }
            else if (other.tag == PERFECT_TAG)
            {
                note.isPerfectTouch = !note.isPerfectTouch;
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.tag == BAD_TAG)
            {
                note.canMove = !note.canMove;
                note.canBeTouch = !note.canBeTouch;
                note.PlayMiss();
            }
            else if (other.tag == GOOD_TAG)
            {
                note.isGoodTouch = !note.isGoodTouch;
            }
            else if (other.tag == PERFECT_TAG)
            {
                note.isPerfectTouch = !note.isPerfectTouch;
            }
        }
    }
}