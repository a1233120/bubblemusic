﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BubbleMusic.GameModule {
    public class GameShooter : MonoBehaviour, IShooter
    {        
        public GameObject note;                 // 音符物件
        private const int RECIVER_COUNTS = 4;   // 儲存接收器的個數
        private Vector3[] reciverList;          // 儲存接收器的座標
        private bool canShoot;                  // 是否可以開始射擊
        public GameSheetBuilder sheetBuilder;   // 譜面建置器

        private void Awake()
        {
            reciverList = new Vector3[RECIVER_COUNTS];
            canShoot = false;
        }

        public void Shoot(string[] nodes, float beat)
        {
            if (!canShoot)
                return;

            for (int i = 0; i < nodes.Length; i++)
            {
                if (int.Parse( nodes[i] ) == 1)
                {
                    GameObject u = GameObject.Instantiate(note, gameObject.transform.position, Quaternion.identity);
                    INote node = u.transform.GetComponent<INote>();
                    node.SetBeat(beat);
                    node.SetSheetBuilder(sheetBuilder);
                    node.SetTargetPos(reciverList[i]);
                }
            }
        }

        public void SetReciver(IReciver[] recivers)
        {
            for (int i = 0; i < reciverList.Length; i++)
            {
                reciverList[i] = recivers[i].GetPosition();
            }        
        }

        public void SetSheetBuilder(GameSheetBuilder sheetBuilder)
        {
            this.sheetBuilder = sheetBuilder;
        }

        public void StartShoot()
        {
            canShoot = true;
        }

        public void EndShoot()
        {
            canShoot = false;
        }
    }
}