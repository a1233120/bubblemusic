﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BubbleMusic.GameModule {
    public class GameEffectFactory : IGameEffectFactory
    {
        private Queue<GameObject> PerfectNodes;
        private Queue<GameObject> GoodNodes;
        private Queue<GameObject> MissNodes;
        private Queue<GameObject> BadNodes;
        private Queue<GameObject> BrokenNodes;
        private const int EFFECT_COUNT = 20;
        private const string GOOD_NODE_PATH = "Module/Game/GoodNode";
        private const string PERFECT_NODE_PATH = "Module/Game/PerfectNode";
        private const string MISS_NODE_PATH = "Module/Game/MissNode";
        private const string BAD_NODE_PATH = "Module/Game/BadNode";
        private const string BROKEN_NODE_PATH = "Module/Game/BrokenBubble";
        public static IGameEffectFactory instance;

        public static IGameEffectFactory CreateFactory()
        {
            if (instance == null)
            {
                instance = new GameEffectFactory();
            }
            return instance;
        }

        private GameEffectFactory()
        {
            // 建立特效池
            GoodNodes = new Queue<GameObject>();
            for (int i = 0; i < EFFECT_COUNT; i++)
            {
                GameObject u = Resources.Load<GameObject>(GOOD_NODE_PATH);
                GameObject cu = GameObject.Instantiate(u);
                cu.transform.position = new Vector3(1000,1000,0);
                GoodNodes.Enqueue(cu);
            }

            MissNodes = new Queue<GameObject>();
            for (int i = 0; i < EFFECT_COUNT; i++)
            {
                GameObject u = Resources.Load<GameObject>(MISS_NODE_PATH);
                GameObject cu = GameObject.Instantiate(u);
                cu.transform.position = new Vector3(1000, 1000, 0);
                MissNodes.Enqueue(cu);
            }

            BadNodes = new Queue<GameObject>();
            for (int i = 0; i < EFFECT_COUNT; i++)
            {
                GameObject u = Resources.Load<GameObject>(BAD_NODE_PATH);
                GameObject cu = GameObject.Instantiate(u);
                cu.transform.position = new Vector3(1000, 1000, 0);
                BadNodes.Enqueue(cu);
            }

            PerfectNodes = new Queue<GameObject>();
            for (int i = 0; i < EFFECT_COUNT; i++)
            {
                GameObject u = Resources.Load<GameObject>(PERFECT_NODE_PATH);
                GameObject cu = GameObject.Instantiate(u);
                cu.transform.position = new Vector3(1000, 1000, 0);
                PerfectNodes.Enqueue(cu);
            }

            BrokenNodes = new Queue<GameObject>();
            for (int i = 0; i < EFFECT_COUNT; i++)
            {
                GameObject u = Resources.Load<GameObject>(BROKEN_NODE_PATH);
                GameObject cu = GameObject.Instantiate(u);
                cu.transform.position = new Vector3(1000, 1000, 0);
                BrokenNodes.Enqueue(cu);
            }
        }

        public void PlayBad(Vector3 vector3, GameObject gameObject)
        {
            GameObject effect = BadNodes.Dequeue();            
            effect.transform.position = vector3;
            effect.GetComponent<IEffectNode>().PlayBad(gameObject);
            BadNodes.Enqueue(effect);
        }

        public void PlayGood(Vector3 vector3, GameObject gameObject)
        {
            GameObject effect = GoodNodes.Dequeue();
            effect.transform.position = vector3;
            effect.GetComponent<IEffectNode>().PlayGood(gameObject);
            GoodNodes.Enqueue(effect);
        }

        public void PlayMiss(Vector3 vector3, GameObject gameObject)
        {
            GameObject effect = MissNodes.Dequeue();
            effect.transform.position = vector3;
            effect.GetComponent<IEffectNode>().PlayMiss(gameObject);
            MissNodes.Enqueue(effect);
        }

        public void PlayPerfect(Vector3 vector3, GameObject gameObject)
        {
            GameObject effect = PerfectNodes.Dequeue();
            effect.transform.position = vector3;
            effect.GetComponent<IEffectNode>().PlayPerfect(gameObject);
            PerfectNodes.Enqueue(effect);
        }

        public void PlayBroken(Vector3 vector3, GameObject gameObject)
        {
            GameObject effect = BrokenNodes.Dequeue();
            effect.transform.position = vector3;
            effect.GetComponent<IEffectNode>().PlayBroken(gameObject);
            BrokenNodes.Enqueue(effect);
        }
    }
}