﻿using UnityEngine;
using System.Timers;
using System;

namespace BubbleMusic.GameModule {
    public class GameController : IModule, IEventListener
    {        
        public enum STATE {
            NONE = 0,       // 尚未執行遊戲
            CREATE = 1,     // 建立該有的物件
            BUILD = 2,      // 遊戲初始化就定位
            PREPARE = 3,    // 到數準備
            PLAY = 4,       // 開始遊完
            END = 5,        // 遊完結束
            SHOWRESULT = 6  // 顯示結算
        }

        public static GameController instance { get; private set; } = null;
        private STATE m_state;                  // 狀態
        private UIMangerHandler m_uimanager;    // uiMangager
        private IAudioManger audioManger;       // audioManger
        private EventMangerHandler m_eventManager; // eventManager
        private bool isEntering = false;        // 狀態機第一次進入狀態的旗標
        private IShooter m_shoot;               // 發射器
        private IReciver[] m_recivers;          // 接收器
        private ISheetBuilder m_sheetBuilder;   // 譜面器
        private IGameItemFactory factory;       // 採用抽象注入
        private string gameMusic;     // 音樂
        private int bpm;
        private float m_loadingDT;

        public static GameController CreateModule()
        {
            if (instance == null)
            {
                instance = new GameController(
                    new GameItemFactory(), 
                    UIManger.instance, 
                    AudioManger.instance, 
                    EventManger.instance);
            }

            return instance;
        }

        private GameController(IGameItemFactory factory, UIMangerHandler uIManger, IAudioManger audioManger, EventMangerHandler eventManger)
        {
            this.m_uimanager = uIManger;
            this.m_eventManager = eventManger;
            this.m_state = STATE.NONE;
            this.factory = factory;
            this.audioManger = audioManger;
            this.m_recivers = new IReciver[4];
            this.bpm = 0;
            this.m_loadingDT = 0;
        }

        public void OnStop()
        {
            // 對建造出來的東西進行移除
        }

        public void EnterGame(string gameMusic)
        {
            m_uimanager.Show(Define.UIName.Loading);
            this.gameMusic = gameMusic;
            if (m_state == STATE.NONE)
            {
                m_state = STATE.CREATE;
                isEntering = true;
                return;
            }

            m_state = STATE.BUILD;
            isEntering = true;
        }

        private void CreateGame()
        {            
            if (m_shoot == null)
            {
                m_shoot = factory.CreateGameItem("Shoot").GetComponent<IShooter>();                
                for (int i = 0; i < 4; i++)
                {
                    m_recivers[i] = factory.CreateGameItem("Reciver").GetComponent<IReciver>();
                    m_recivers[i].SetPosition(ReciverMap(i));
                }

                m_sheetBuilder = factory.CreateGameItem("SheetBuilder").GetComponent<ISheetBuilder>();
            }                     
            m_state = STATE.BUILD;
            isEntering = true;
        }

        private void BuildGame()
        {
            GameEffectFactory.CreateFactory();
            m_shoot.SetReciver(m_recivers);
            m_shoot.SetSheetBuilder(m_sheetBuilder as GameSheetBuilder);
            m_sheetBuilder.SetShooter(m_shoot);
            m_sheetBuilder.ResetTime();
            m_state = STATE.PREPARE;
            isEntering = true;
        }

        private void PreparedGame()
        { 
            m_sheetBuilder.ReadSheet(gameMusic);
            m_uimanager.Hide(Define.UIName.Loading);
            m_state = STATE.PLAY;
            isEntering = true;
        }

        public void PlayGame()
        {            
            audioManger.PlayGameMusic(gameMusic.ToString()
                , (Sound sound) => {
                    if (sound.finished)
                    {
                        m_state = STATE.END;
                        isEntering = true;
                    }
                });
            m_shoot.StartShoot();
            m_sheetBuilder.StartSheet();
        }

        public void PauseGame()
        {
            Time.timeScale = 0;
            audioManger.PauseGameMusic();
            m_shoot.EndShoot();
            m_sheetBuilder.Pause();
        }

        public void ResumeGame()
        {
            Time.timeScale = 1;
            audioManger.ResumeGameMusic();
            m_shoot.StartShoot();
            m_sheetBuilder.Resume();
        }

        private void CheckGame()
        {
            
        }

        public void EndGame()
        {
            audioManger.StopGameMusic();
            m_shoot.EndShoot();
            m_sheetBuilder.Stop();
            m_eventManager.PostNotification(Define.EventType.GameEnd);
            m_state = STATE.SHOWRESULT;
            isEntering = true;
        }

        public void ShowResult()
        {
            // 顯示結算畫面            
            UIManger.instance.Show(Define.UIName.Result);
            // todo 之後做在結算畫面內            
            Debug.Log("is end game");            
        }

        public void ClearAll()
        {
            GameObject[] gameObjects = UnityEngine.SceneManagement.SceneManager.GetActiveScene().GetRootGameObjects();
            for (int i = 0; i < gameObjects.Length; i++)
            {
                if (gameObjects[i].transform.tag == "note")
                {
                    UnityEngine.Object.Destroy(gameObjects[i]);
                }
            }
        }

        public void OnUpdate(float deltaTime)
        {
            // 如有需要每幀都進行偵測的物件可以在這邊傳遞訊息
            switch (m_state)
            {
                case STATE.NONE:
                    break;
                case STATE.CREATE:
                    if (isEntering)
                    {                        
                        isEntering = false;
                        CreateGame();
                    }
                    break;
                case STATE.BUILD:
                    if (isEntering)
                    {                        
                        isEntering = false;
                        BuildGame();
                    }
                    break;
                case STATE.PREPARE:
                    if (isEntering)
                    {
                        isEntering = false;                        
                                               
                    }
                    m_loadingDT += deltaTime;
                    if (m_loadingDT > 1.5f)
                    {
                        m_loadingDT = 0;
                        PreparedGame();

                    }
                    break;
                case STATE.PLAY:
                    if (isEntering)
                    {
                        isEntering = false;
                        m_uimanager.Show(Define.UIName.Ready);
                    }
                    break;
                case STATE.END:
                    if (isEntering)
                    {
                        isEntering = false;
                        EndGame();
                    }
                    break;
                case STATE.SHOWRESULT:
                    if (isEntering)
                    {
                        isEntering = false;
                        ShowResult();
                    }
                    break;
                default:
                    break;
            }
        }

        public void OnEvent(Define.EventType eventType)
        {
            // 事件傳遞接口
        }

        private Vector3 ReciverMap(int index)
        {
            switch (index)
            {
                case 0:
                    return new Vector3(-7.0f, -1.6f, 0.2f);
                case 1:
                    return new Vector3(-3.0f, -3.5f, 0.2f);
                case 2:
                    return new Vector3(3.0f, -3.5f, 0.2f);
                case 3:
                    return new Vector3(7.0f, -1.6f, 0.2f);
                default:
                    break;
            }
            return new Vector3(0, 0);
        }

        private Vector3 BadReciverMap(int index)
        {
            switch (index)
            {
                case 0:
                    return new Vector3(-7.6f, -2.5f, 0.2f);
                case 1:
                    return new Vector3(-3.5f, -4.6f, 0.2f);
                case 2:
                    return new Vector3(3.5f, -4.6f, 0.2f);
                case 3:
                    return new Vector3(7.6f, -2.5f, 0.2f);
                default:
                    break;
            }
            return new Vector3(0, 0);
        }

        public void KeyPress(KeyCode keyCode)
        {
            switch (keyCode)
            {
                case KeyCode.Q:
                    if(m_recivers[0] != null)
                        m_recivers[0].OnPress();
                    break;
                case KeyCode.W:
                    if (m_recivers[1] != null)
                        m_recivers[1].OnPress();
                    break;
                case KeyCode.O:
                    if (m_recivers[2] != null)
                        m_recivers[2].OnPress();
                    break;
                case KeyCode.P:
                    if (m_recivers[3] != null)
                        m_recivers[3].OnPress();
                    break;
            }
        }
    }
}