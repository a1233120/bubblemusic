﻿namespace BubbleMusic.GameModule
{
    public interface IReciver
    {
        UnityEngine.Vector3 GetPosition();
        void SetPosition(UnityEngine.Vector3 vector3);
        void OnPress();
    }
}