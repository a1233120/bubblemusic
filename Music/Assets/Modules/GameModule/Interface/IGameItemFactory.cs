﻿namespace BubbleMusic
{
    public interface IGameItemFactory
    {
        // path : Resources/Modules/Game/
        UnityEngine.Transform CreateGameItem(string name); 
    }
}