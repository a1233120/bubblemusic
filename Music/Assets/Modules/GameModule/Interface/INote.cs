﻿namespace BubbleMusic.GameModule
{
    public interface INote
    {
        void SetSheetBuilder(GameSheetBuilder sheetBuilder);
        void SetTargetPos(UnityEngine.Vector3 vector3);
        void SetBeat(float beat);
        void OnTouchBegin();
    }
}