﻿namespace BubbleMusic.GameModule
{
    public interface ISheetBuilder
    {
        void ReadSheet(string gameMusic);
        void StartSheet();
        void SetShooter(IShooter shooter);
        void ResetTime();
        void Pause();
        void Resume();
        void Stop();
    }
}
