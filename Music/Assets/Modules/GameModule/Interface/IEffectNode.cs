﻿namespace BubbleMusic.GameModule
{
    public interface IEffectNode
    {
        void PlayBad(UnityEngine.GameObject gameObject);
        void PlayGood(UnityEngine.GameObject gameObject);
        void PlayPerfect(UnityEngine.GameObject gameObject);
        void PlayMiss(UnityEngine.GameObject gameObject);
        void PlayBroken(UnityEngine.GameObject gameObject);
    }
}