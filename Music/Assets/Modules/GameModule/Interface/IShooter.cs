﻿namespace BubbleMusic.GameModule
{
    public interface IShooter
    {
        void Shoot(string[] nodes, float beat);
        void SetReciver(IReciver[] recivers);
        void StartShoot();
        void EndShoot();
        void SetSheetBuilder(GameSheetBuilder sheetBuilder);
    }
}