﻿namespace BubbleMusic.GameModule {
    public interface IGameEffectFactory
    {
        void PlayPerfect(UnityEngine.Vector3 vector3, UnityEngine.GameObject gameObject);
        void PlayGood(UnityEngine.Vector3 vector3, UnityEngine.GameObject gameObject);
        void PlayBad(UnityEngine.Vector3 vector3, UnityEngine.GameObject gameObject);
        void PlayMiss(UnityEngine.Vector3 vector3, UnityEngine.GameObject gameObject);
        void PlayBroken(UnityEngine.Vector3 vector3, UnityEngine.GameObject gameObject);
    }
}