﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Linq;
using UnityEngine;

namespace BubbleMusic.GameModule {
    public class GameSheetBuilder : MonoBehaviour, ISheetBuilder
    {
        private FileMangerHandler fileManger;
        private IShooter shooter;
        public float BeatsShownOnScreen = 5.0f; //一次能有多少個音符在場上
        private Sheet sheet;                    //譜面格式
        public float songPosition = 0.0f;       //歌曲的進度  
        public float songPosInBeats = 0.0f;     //節拍當前位置        
        public float secPerBeat = 0.0f;         //每個節拍的時間
        public float dsptimesong = 0.0f;
        public float pausetimes = 0.0f;         // 暫停的時間點
        public float bpm = 0;                 //譜面的速度
        public int nextIndex = 0;               //下一個音符的INDEX
        public float[] notes;                   //歌曲的譜面
        private bool canStart = false;          //是否開始了

        private void Awake()
        {
            this.fileManger = FileManger.instance;
        }

        private void Update()
        {
            if (!canStart)
                return;

            songPosition = (float)(AudioSettings.dspTime - dsptimesong);
            songPosInBeats = songPosition / secPerBeat;
#if DEBUG_MODE
            Debug.Log("songPosition: " + songPosition);
            Debug.Log("songPosInBeats: " + songPosInBeats);
            Debug.Log("nextIndex: " + nextIndex);
            Debug.Log("songPosInBeats :" + songPosInBeats);
            Debug.Log("BeatsShownOnScreen :" + BeatsShownOnScreen);
            Debug.Log("songPosInBeats + BeatsShownOnScreen :" + songPosInBeats + BeatsShownOnScreen);
#endif
            if (nextIndex < notes.Length && notes[nextIndex] < songPosInBeats + BeatsShownOnScreen)
            {                
                FindBeatsData(notes[nextIndex]);
                nextIndex++;
            }
        }

        public void ReadSheet(string gameMusic)
        {
            sheet = new Sheet();
            XElement f = fileManger.ReadXmlFile(gameMusic.ToString());
            foreach (var item in f.Elements())
            {                
                float time = float.Parse(item.Element("time").Value.Trim());
                string str = item.Element("beats").Value.Trim();
                string[] strArray = str.Split(',');
                sheet.SetBeats(time, strArray);
            }

            XElement d = fileManger.ReadNotesXmlFile(gameMusic.ToString());
            string notestr = d.Value.Trim();
            string[] temp = notestr.Split(',');
            notes = new float[temp.Length];
            for (int i = 0; i < notes.Length; i++)
            {
                notes[i] = float.Parse(temp[i]);
            }

            XElement e = fileManger.ReadInfoXmlFile(gameMusic.ToString());
            this.bpm = int.Parse(e.Element("bpm").Value.Trim());

        }

        public void Pause()
        {
            canStart = false;
            pausetimes = (float)AudioSettings.dspTime;
        }

        public void ResetTime()
        {
            songPosition = 0.0f;       //歌曲的進度  
            songPosInBeats = 0.0f;     //節拍當前位置        
            secPerBeat = 0.0f;         //每個節拍的時間
            dsptimesong = 0.0f;
            pausetimes = 0.0f;
            bpm = 0;                 //譜面的速度
            nextIndex = 0;
        }

        public void Resume()
        {
            canStart = true;
            dsptimesong += (float)AudioSettings.dspTime - pausetimes;
            pausetimes = 0.0f;
        }

        public void StartSheet()
        {
            secPerBeat = 60f / bpm;
            dsptimesong = (float)AudioSettings.dspTime;
            canStart = true;
        }

        public void SetShooter(IShooter shooter)
        {
            this.shooter = shooter;
        }

        private void FindBeatsData(float beat)
        {
            string[] u = sheet.GetNowNodes(beat);
            shooter.Shoot(u, beat);
        }

        private void SetBpm(int bpm)
        {
            if (bpm <= 0)
                return;
            this.bpm = bpm;
        }

        public void Stop()
        {
            canStart = false;
            GameObject[] gameObjects = UnityEngine.SceneManagement.SceneManager.GetActiveScene().GetRootGameObjects();
            for (int i = 0; i < gameObjects.Length; i++)
            {
                if (gameObjects[i].transform.tag == "note")
                {
                    Destroy(gameObjects[i]);
                }
            }
        }
    }

    public class Sheet
    {
        Dictionary<float, string[]> u;

        public Sheet()
        {
            u = new Dictionary<float, string[]>();
        }

        public void SetBeats(float time, string[] nodes)
        {
            u.Add(time, nodes);
        }

        public string[] GetNowNodes(float time)
        {
            string[] nodes;
            if (u.TryGetValue(time, out nodes))
            {
                return nodes;
            }
            else
            {
                nodes = new string[4];
                for (int i = 0; i < nodes.Length; i++)
                {
                    nodes[i] = "0";
                }
            }
            return nodes;
        }
    }
}