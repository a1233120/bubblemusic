﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BubbleMusic.RankModule
{
    public class GameCombo : IGameCombo
    {
        private int badHits;
        private int goodHits;
        private int missHits;
        private int perfectHits;
        private int combo;
        private int highcombo;

        public GameCombo()
        {
            badHits = 0;
            goodHits = 0;
            missHits = 0;
        }

        public void AddBadHits()
        {
            badHits++;
        }

        public void AddCombo()
        {
            combo++;
            EventManger.instance.PostNotification(Define.EventType.UpdateCombo);
        }

        public void AddPerfectHits()
        {
            perfectHits++;
        }

        public void AddGoodHits()
        {
            goodHits++;
        }

        public void AddMissHits()
        {
            missHits++;
        }

        public int GetBadHits()
        {
            return badHits;
        }

        public int GetPerfectHits()
        {
            return perfectHits;
        }

        public int GetGoodHits()
        {
            return goodHits;
        }

        public int GetMissHits()
        {
            return missHits;
        }

        public int GetTotalCombo()
        {
            return highcombo;
        }

        public void ResetAll()
        {
            badHits = 0;
            goodHits = 0;
            missHits = 0;
            perfectHits = 0;
            combo = 0;
            highcombo = 0;
            EventManger.instance.PostNotification(Define.EventType.UpdateCombo);
        }

        public void ResetCombo()
        {
            if (combo > highcombo)
                highcombo = combo;            
            combo = 0;
            EventManger.instance.PostNotification(Define.EventType.UpdateCombo);
        }

        public int GetNowCombo()
        {
            return combo;
        }
    }
}