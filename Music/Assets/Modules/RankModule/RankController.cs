﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BubbleMusic.RankModule
{
    public class RankController : IModule, IEventListener
    {
        public static RankController instance { get; private set; } = null;
        EventMangerHandler m_eventManger;
        IGameScore m_gameScore;
        IGameCombo m_gameCombo;

        public static RankController CreateModule()
        {
            if (instance == null)
            {
                instance = new RankController(EventManger.instance);
            }

            return instance;
        }

        private RankController(EventMangerHandler eventManger)
        {
            m_eventManger = eventManger;
            m_gameScore = new GameScore(m_eventManger);
            m_gameCombo = new GameCombo();
            this.Init();
        }

        private void Init()
        {
            m_eventManger.AddListener(Define.EventType.ChangeToGame, OnEvent);
            m_eventManger.AddListener(Define.EventType.HitPerfect, OnEvent);
            m_eventManger.AddListener(Define.EventType.HitGood, OnEvent);
            m_eventManger.AddListener(Define.EventType.HitBad, OnEvent);
            m_eventManger.AddListener(Define.EventType.HitMiss, OnEvent);
            m_eventManger.AddListener(Define.EventType.GameEnd, OnEvent);
        }

        public void OnEvent(Define.EventType eventType)
        {
            switch (eventType)
            {
                case Define.EventType.ChangeToGame:
                    m_gameScore.ResetGameScore();
                    m_gameCombo.ResetAll();
                    break;
                case Define.EventType.HitPerfect:
                    m_gameScore.SetGameScore(HitType.PERFECT);
                    m_gameCombo.AddPerfectHits();
                    m_gameCombo.AddCombo();
                    break;
                case Define.EventType.HitGood:
                    m_gameScore.SetGameScore(HitType.GOOD);
                    m_gameCombo.AddGoodHits();
                    m_gameCombo.AddCombo();
                    break;
                case Define.EventType.HitBad:
                    m_gameScore.SetGameScore(HitType.BAD);
                    m_gameCombo.AddBadHits();
                    m_gameCombo.ResetCombo();
                    break;
                case Define.EventType.HitMiss:
                    m_gameScore.SetGameScore(HitType.MISS);
                    m_gameCombo.AddMissHits();
                    m_gameCombo.ResetCombo();
                    break;
                case Define.EventType.GameEnd:
                    m_gameCombo.ResetCombo();
                    break;
                default:
                    break;
            }
        }

        // 獲得遊戲分數
        public int GetGameScore()
        {
            if(m_gameScore != null)
                return m_gameScore.GetGameScore();
            return 0;
        }

        public int GetTotalCombo()
        {
            if (m_gameCombo != null)
                return m_gameCombo.GetTotalCombo();
            return 0;
        }

        public int GetNowCombo()
        {
            if (m_gameCombo != null)
                return m_gameCombo.GetNowCombo();
            return 0;
        }

        public int GetPerfectHits()
        {
            if (m_gameCombo != null)
                return m_gameCombo.GetPerfectHits();
            return 0;
        }

        public int GetGoodHits()
        {
            if (m_gameCombo != null)
                return m_gameCombo.GetGoodHits();
            return 0;
        }

        public int GetBadHits()
        {
            if (m_gameCombo != null)
                return m_gameCombo.GetBadHits();
            return 0;
        }

        public int GetMissHits()
        {
            if (m_gameCombo != null)
                return m_gameCombo.GetMissHits();
            return 0;
        }

        public void OnStop()
        {

        }

        public void OnUpdate(float deltaTime)
        {

        }
    }
}