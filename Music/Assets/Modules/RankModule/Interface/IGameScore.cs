﻿namespace BubbleMusic.RankModule
{
    public enum HitType {
        PERFECT,
        GOOD,
        BAD,
        MISS,        
    }

    public interface IGameScore
    {
        void SetGameScore(HitType hitType);
        int GetGameScore();
        void ResetGameScore();
    }
}