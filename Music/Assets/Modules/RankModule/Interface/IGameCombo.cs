﻿namespace BubbleMusic.RankModule
{
    public interface IGameCombo
    {
        void AddGoodHits();
        void AddBadHits();
        void AddMissHits();
        void AddPerfectHits();
        void AddCombo();
        void ResetCombo();
        void ResetAll();
        int GetTotalCombo();
        int GetGoodHits();
        int GetBadHits();
        int GetMissHits();
        int GetPerfectHits();
        int GetNowCombo();
    }
}