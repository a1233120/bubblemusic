﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BubbleMusic.RankModule
{    
    public class GameScore : IGameScore
    {
        private int m_gameScore;
        private EventMangerHandler m_eventManager;

        public GameScore(EventMangerHandler eventManager)
        {
            this.m_gameScore = 0;
            this.m_eventManager = eventManager;
        }

        public int GetGameScore()
        {
            return this.m_gameScore;
        }

        public void ResetGameScore()
        {
            this.m_gameScore = 0;
            this.m_eventManager.PostNotification(Define.EventType.UpdateScore);
        }

        public void SetGameScore(HitType hitType)
        {
            switch (hitType)
            {
                case HitType.PERFECT:
                    this.m_gameScore += 200;
                    break;
                case HitType.GOOD:
                    this.m_gameScore += 100;
                    break;
                case HitType.BAD:
                    this.m_gameScore += 0;
                    break;
                case HitType.MISS:
                    this.m_gameScore += 0;
                    break;
                default:
                    break;
            }

            this.m_eventManager.PostNotification(Define.EventType.UpdateScore);
        }
    }
}