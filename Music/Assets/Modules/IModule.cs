﻿namespace BubbleMusic
{
    public interface IModule
    {
        //void OnStart();
        //void OnEnter();
        void OnUpdate(float deltaTime);
        void OnStop();
    }
}
